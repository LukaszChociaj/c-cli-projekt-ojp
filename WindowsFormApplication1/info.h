#pragma once

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for info
	/// </summary>
	public ref class info : public System::Windows::Forms::Form
	{
	public:
		info(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~info()
		{
			if (components)
			{
				delete components;
			}
		}
	internal: System::Windows::Forms::Label^  labelinfo;
	protected: 

	protected: 

	protected: 

	protected: 

	protected: 


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(info::typeid));
			this->labelinfo = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// labelinfo
			// 
			this->labelinfo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->labelinfo->Location = System::Drawing::Point(1, 9);
			this->labelinfo->MaximumSize = System::Drawing::Size(300, 300);
			this->labelinfo->MinimumSize = System::Drawing::Size(400, 400);
			this->labelinfo->Name = L"labelinfo";
			this->labelinfo->Size = System::Drawing::Size(400, 400);
			this->labelinfo->TabIndex = 0;
			this->labelinfo->Text = resources->GetString(L"labelinfo.Text");
			// 
			// info
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(395, 110);
			this->Controls->Add(this->labelinfo);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(411, 149);
			this->MinimumSize = System::Drawing::Size(411, 149);
			this->Name = L"info";
			this->Text = L"O programie";
			this->ResumeLayout(false);

		}
#pragma endregion

	};
}
