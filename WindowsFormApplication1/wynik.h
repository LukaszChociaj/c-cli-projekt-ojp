#pragma once

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for wynik
	/// </summary>
	public ref class wynik : public System::Windows::Forms::Form
	{
	public:
		wynik(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~wynik()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;

	public: System::Windows::Forms::Label^  lkcal;
	public: System::Windows::Forms::Label^  lwegle;
	public: System::Windows::Forms::Label^  lbialko;
	public: System::Windows::Forms::Label^  ltluszcze;
	public: System::Windows::Forms::Label^  gdymasamniejsza;
	private: 



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->lkcal = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->lwegle = (gcnew System::Windows::Forms::Label());
			this->lbialko = (gcnew System::Windows::Forms::Label());
			this->ltluszcze = (gcnew System::Windows::Forms::Label());
			this->gdymasamniejsza = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label1->Location = System::Drawing::Point(-3, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(429, 31);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Codziennie powiniene� spo�ywa�:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label2->Location = System::Drawing::Point(13, 88);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(52, 17);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Kalorie";
			// 
			// lkcal
			// 
			this->lkcal->AutoSize = true;
			this->lkcal->Location = System::Drawing::Point(13, 116);
			this->lkcal->Name = L"lkcal";
			this->lkcal->Size = System::Drawing::Size(35, 13);
			this->lkcal->TabIndex = 2;
			this->lkcal->Text = L"label3";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label3->Location = System::Drawing::Point(97, 88);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(116, 17);
			this->label3->TabIndex = 3;
			this->label3->Text = L"W�glowodany [g]";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label4->Location = System::Drawing::Point(226, 88);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(66, 17);
			this->label4->TabIndex = 4;
			this->label4->Text = L"Bia�ko [g]";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->label5->Location = System::Drawing::Point(320, 88);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(84, 17);
			this->label5->TabIndex = 5;
			this->label5->Text = L"T�uszcze [g]";
			// 
			// lwegle
			// 
			this->lwegle->AutoSize = true;
			this->lwegle->Location = System::Drawing::Point(97, 116);
			this->lwegle->Name = L"lwegle";
			this->lwegle->Size = System::Drawing::Size(35, 13);
			this->lwegle->TabIndex = 6;
			this->lwegle->Text = L"label3";
			// 
			// lbialko
			// 
			this->lbialko->AutoSize = true;
			this->lbialko->Location = System::Drawing::Point(226, 116);
			this->lbialko->Name = L"lbialko";
			this->lbialko->Size = System::Drawing::Size(35, 13);
			this->lbialko->TabIndex = 7;
			this->lbialko->Text = L"label3";
			// 
			// ltluszcze
			// 
			this->ltluszcze->AutoSize = true;
			this->ltluszcze->Location = System::Drawing::Point(320, 116);
			this->ltluszcze->Name = L"ltluszcze";
			this->ltluszcze->Size = System::Drawing::Size(35, 13);
			this->ltluszcze->TabIndex = 8;
			this->ltluszcze->Text = L"label3";
			// 
			// gdymasamniejsza
			// 
			this->gdymasamniejsza->AutoSize = true;
			this->gdymasamniejsza->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->gdymasamniejsza->Location = System::Drawing::Point(12, 173);
			this->gdymasamniejsza->Name = L"gdymasamniejsza";
			this->gdymasamniejsza->Size = System::Drawing::Size(0, 17);
			this->gdymasamniejsza->TabIndex = 9;
			// 
			// wynik
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(430, 234);
			this->Controls->Add(this->gdymasamniejsza);
			this->Controls->Add(this->ltluszcze);
			this->Controls->Add(this->lbialko);
			this->Controls->Add(this->lwegle);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->lkcal);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(446, 273);
			this->MinimumSize = System::Drawing::Size(446, 273);
			this->Name = L"wynik";
			this->Text = L"Co je��";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


};
}
