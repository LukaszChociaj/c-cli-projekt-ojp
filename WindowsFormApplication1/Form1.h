#pragma once
#include"insertm1.h"
#include"wynik.h"
#include "info.h"

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{

	public:
		
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buttonliczacymakro;
	protected: 

	private: System::Windows::Forms::Label^  wagapoczatek;
	private: System::Windows::Forms::Label^  wagadocel;
	private: System::Windows::Forms::Label^  labelm1;
	private: System::Windows::Forms::Label^  labelm2;
	protected: 
	private: System::Windows::Forms::GroupBox^  groupBoxplec;
	private: System::Windows::Forms::Button^  dodajwagapoczatek;
	private: System::Windows::Forms::Button^  Dodajwagakoniec;
	private: System::Windows::Forms::RadioButton^  pleck;
	private: System::Windows::Forms::RadioButton^  plecm;
	private: System::Windows::Forms::GroupBox^  groupBoxaktywonsc;
	private: System::Windows::Forms::RadioButton^  akd;
	private: System::Windows::Forms::RadioButton^  aks;
	private: System::Windows::Forms::RadioButton^  akm;
	private: System::Windows::Forms::RadioButton^  aknn;
	private: System::Double wspolczynnik;
	private: System::Windows::Forms::Button^  button1;
	private: System::Double wskobieta;

	private:
		void setwsp(System::Double i){
		wspolczynnik=i;
		}
		
		System::Double getwsp(){
		return wspolczynnik;
		}

		void setwsk(System::Double i){
		wskobieta=i;
		}

		System::Double getwspk(){
		return wspolczynnik;
		}


		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->buttonliczacymakro = (gcnew System::Windows::Forms::Button());
			this->wagapoczatek = (gcnew System::Windows::Forms::Label());
			this->wagadocel = (gcnew System::Windows::Forms::Label());
			this->labelm1 = (gcnew System::Windows::Forms::Label());
			this->labelm2 = (gcnew System::Windows::Forms::Label());
			this->groupBoxplec = (gcnew System::Windows::Forms::GroupBox());
			this->pleck = (gcnew System::Windows::Forms::RadioButton());
			this->plecm = (gcnew System::Windows::Forms::RadioButton());
			this->dodajwagapoczatek = (gcnew System::Windows::Forms::Button());
			this->Dodajwagakoniec = (gcnew System::Windows::Forms::Button());
			this->groupBoxaktywonsc = (gcnew System::Windows::Forms::GroupBox());
			this->aknn = (gcnew System::Windows::Forms::RadioButton());
			this->akd = (gcnew System::Windows::Forms::RadioButton());
			this->aks = (gcnew System::Windows::Forms::RadioButton());
			this->akm = (gcnew System::Windows::Forms::RadioButton());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->groupBoxplec->SuspendLayout();
			this->groupBoxaktywonsc->SuspendLayout();
			this->SuspendLayout();
			// 
			// buttonliczacymakro
			// 
			this->buttonliczacymakro->Location = System::Drawing::Point(305, 123);
			this->buttonliczacymakro->Name = L"buttonliczacymakro";
			this->buttonliczacymakro->Size = System::Drawing::Size(120, 50);
			this->buttonliczacymakro->TabIndex = 0;
			this->buttonliczacymakro->Text = L"Licz makrosk�adniki";
			this->buttonliczacymakro->UseVisualStyleBackColor = true;
			this->buttonliczacymakro->Click += gcnew System::EventHandler(this, &Form1::buttonliczacymakro_Click);
			// 
			// wagapoczatek
			// 
			this->wagapoczatek->AutoSize = true;
			this->wagapoczatek->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->wagapoczatek->Location = System::Drawing::Point(105, 45);
			this->wagapoczatek->Name = L"wagapoczatek";
			this->wagapoczatek->Size = System::Drawing::Size(107, 22);
			this->wagapoczatek->TabIndex = 2;
			this->wagapoczatek->Text = L"Twoja waga";
			// 
			// wagadocel
			// 
			this->wagadocel->AutoSize = true;
			this->wagadocel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 13, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->wagadocel->Location = System::Drawing::Point(80, 89);
			this->wagadocel->Name = L"wagadocel";
			this->wagadocel->Size = System::Drawing::Size(138, 22);
			this->wagadocel->TabIndex = 3;
			this->wagadocel->Text = L"Waga docelowa";
			// 
			// labelm1
			// 
			this->labelm1->AutoSize = true;
			this->labelm1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->labelm1->Location = System::Drawing::Point(228, 45);
			this->labelm1->Name = L"labelm1";
			this->labelm1->Size = System::Drawing::Size(0, 20);
			this->labelm1->TabIndex = 6;
			// 
			// labelm2
			// 
			this->labelm2->AutoSize = true;
			this->labelm2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(238)));
			this->labelm2->Location = System::Drawing::Point(228, 89);
			this->labelm2->Name = L"labelm2";
			this->labelm2->Size = System::Drawing::Size(0, 20);
			this->labelm2->TabIndex = 7;
			// 
			// groupBoxplec
			// 
			this->groupBoxplec->Controls->Add(this->pleck);
			this->groupBoxplec->Controls->Add(this->plecm);
			this->groupBoxplec->Location = System::Drawing::Point(379, 41);
			this->groupBoxplec->Name = L"groupBoxplec";
			this->groupBoxplec->Size = System::Drawing::Size(123, 70);
			this->groupBoxplec->TabIndex = 16;
			this->groupBoxplec->TabStop = false;
			this->groupBoxplec->Text = L"P�e�";
			// 
			// pleck
			// 
			this->pleck->AutoSize = true;
			this->pleck->Location = System::Drawing::Point(7, 44);
			this->pleck->Name = L"pleck";
			this->pleck->Size = System::Drawing::Size(61, 17);
			this->pleck->TabIndex = 1;
			this->pleck->TabStop = true;
			this->pleck->Text = L"Kobieta";
			this->pleck->UseVisualStyleBackColor = true;
			// 
			// plecm
			// 
			this->plecm->AutoSize = true;
			this->plecm->Location = System::Drawing::Point(7, 20);
			this->plecm->Name = L"plecm";
			this->plecm->Size = System::Drawing::Size(78, 17);
			this->plecm->TabIndex = 0;
			this->plecm->TabStop = true;
			this->plecm->Text = L"M�czyzna";
			this->plecm->UseVisualStyleBackColor = true;
			// 
			// dodajwagapoczatek
			// 
			this->dodajwagapoczatek->Location = System::Drawing::Point(270, 39);
			this->dodajwagapoczatek->Name = L"dodajwagapoczatek";
			this->dodajwagapoczatek->Size = System::Drawing::Size(85, 34);
			this->dodajwagapoczatek->TabIndex = 17;
			this->dodajwagapoczatek->Text = L"Dodaj";
			this->dodajwagapoczatek->UseVisualStyleBackColor = true;
			this->dodajwagapoczatek->Click += gcnew System::EventHandler(this, &Form1::dodajwagapoczatek_Click);
			// 
			// Dodajwagakoniec
			// 
			this->Dodajwagakoniec->Location = System::Drawing::Point(270, 77);
			this->Dodajwagakoniec->Name = L"Dodajwagakoniec";
			this->Dodajwagakoniec->Size = System::Drawing::Size(85, 34);
			this->Dodajwagakoniec->TabIndex = 18;
			this->Dodajwagakoniec->Text = L"Dodaj";
			this->Dodajwagakoniec->UseVisualStyleBackColor = true;
			this->Dodajwagakoniec->Click += gcnew System::EventHandler(this, &Form1::Dodajwagakoniec_Click);
			// 
			// groupBoxaktywonsc
			// 
			this->groupBoxaktywonsc->Controls->Add(this->aknn);
			this->groupBoxaktywonsc->Controls->Add(this->akd);
			this->groupBoxaktywonsc->Controls->Add(this->aks);
			this->groupBoxaktywonsc->Controls->Add(this->akm);
			this->groupBoxaktywonsc->Location = System::Drawing::Point(528, 39);
			this->groupBoxaktywonsc->Name = L"groupBoxaktywonsc";
			this->groupBoxaktywonsc->Size = System::Drawing::Size(200, 107);
			this->groupBoxaktywonsc->TabIndex = 19;
			this->groupBoxaktywonsc->TabStop = false;
			this->groupBoxaktywonsc->Text = L"Aktywno�� w ci�gu dnia";
			// 
			// aknn
			// 
			this->aknn->AutoSize = true;
			this->aknn->Location = System::Drawing::Point(6, 61);
			this->aknn->Name = L"aknn";
			this->aknn->Size = System::Drawing::Size(144, 17);
			this->aknn->TabIndex = 3;
			this->aknn->TabStop = true;
			this->aknn->Text = L"du�a ( praca na nogach )";
			this->aknn->UseVisualStyleBackColor = true;
			// 
			// akd
			// 
			this->akd->AutoSize = true;
			this->akd->Location = System::Drawing::Point(6, 84);
			this->akd->Name = L"akd";
			this->akd->Size = System::Drawing::Size(166, 17);
			this->akd->TabIndex = 2;
			this->akd->TabStop = true;
			this->akd->Text = L"bardzo du�a ( praca fizyczna )";
			this->akd->UseVisualStyleBackColor = true;
			// 
			// aks
			// 
			this->aks->AutoSize = true;
			this->aks->Location = System::Drawing::Point(7, 38);
			this->aks->Name = L"aks";
			this->aks->Size = System::Drawing::Size(59, 17);
			this->aks->TabIndex = 1;
			this->aks->TabStop = true;
			this->aks->Text = L"�rednia";
			this->aks->UseVisualStyleBackColor = true;
			// 
			// akm
			// 
			this->akm->AutoSize = true;
			this->akm->Location = System::Drawing::Point(7, 16);
			this->akm->Name = L"akm";
			this->akm->Size = System::Drawing::Size(133, 17);
			this->akm->TabIndex = 0;
			this->akm->TabStop = true;
			this->akm->Text = L"ma�a ( praca siedz�ca)";
			this->akm->UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(667, 10);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 20;
			this->button1->Text = L"O programie";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"$this.BackgroundImage")));
			this->ClientSize = System::Drawing::Size(754, 411);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->groupBoxaktywonsc);
			this->Controls->Add(this->Dodajwagakoniec);
			this->Controls->Add(this->dodajwagapoczatek);
			this->Controls->Add(this->groupBoxplec);
			this->Controls->Add(this->labelm2);
			this->Controls->Add(this->labelm1);
			this->Controls->Add(this->wagadocel);
			this->Controls->Add(this->wagapoczatek);
			this->Controls->Add(this->buttonliczacymakro);
			this->MaximizeBox = false;
			this->MaximumSize = System::Drawing::Size(770, 450);
			this->MinimumSize = System::Drawing::Size(770, 450);
			this->Name = L"Form1";
			this->Text = L"BeFit";
			this->groupBoxplec->ResumeLayout(false);
			this->groupBoxplec->PerformLayout();
			this->groupBoxaktywonsc->ResumeLayout(false);
			this->groupBoxaktywonsc->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void dodajwagapoczatek_Click(System::Object^  sender, System::EventArgs^  e) {
			insertm1^ oknom1 = gcnew insertm1();
			if(oknom1->ShowDialog() == System::Windows::Forms::DialogResult::OK){
			this->labelm1->Text = oknom1->textBoxm->Text;
			
			}
		}
private: System::Void Dodajwagakoniec_Click(System::Object^  sender, System::EventArgs^  e) {
			insertm1^ oknom2 = gcnew insertm1();
			oknom2->Text="Podaj docelow� wag�";
			if(oknom2->ShowDialog() == System::Windows::Forms::DialogResult::OK){
			this->labelm2->Text = oknom2->textBoxm->Text;
			}
		}
private: System::Void buttonliczacymakro_Click(System::Object^  sender, System::EventArgs^  e) {
			wynik^ oknowynik = gcnew wynik();
		
			if(plecm->Checked==true){
				try{			
					if(akm->Checked==true){
					setwsp(1.1);
					}
					else if(aks->Checked==true){
						setwsp(1.2);
					}
					else if(aknn->Checked==true){
						setwsp(1.4);
					}
					else if(akd->Checked==true){
						setwsp(1.5);
					}
					else{
						setwsp(1);
						MessageBox::Show("Wybierz rodzaj aktywno�ci, w ci�gu dnia.");
					}
					System::Int16 m1 = Convert::ToInt16(labelm1->Text);
					System::Int16 m2 = Convert::ToInt16(labelm2->Text);
				
					if(m1<m2){						
						oknowynik->lkcal->Text=Convert::ToString(m1*24*this->wspolczynnik+500);
						oknowynik->gdymasamniejsza->Text="Masa nie ro�nie? Dodaj kalori :)";
					}
					else if(m1>m2){
						oknowynik->lkcal->Text=Convert::ToString(m1*24*this->wspolczynnik-300);
						oknowynik->gdymasamniejsza->Text="Podczas zrzucania wagi pami�taj o treningu ruchowym!";
					}
					else{
						oknowynik->lkcal->Text=Convert::ToString(m1*24*this->wspolczynnik);
					}

					System::Double kalorie = Convert::ToDouble(oknowynik->lkcal->Text);
					oknowynik->lwegle->Text=Convert::ToString(Convert::ToInt16(kalorie/8));
					oknowynik->lbialko->Text=Convert::ToString(Convert::ToInt16(kalorie*0.3/4));
					oknowynik->ltluszcze->Text=Convert::ToString(Convert::ToInt16(kalorie*0.2/9));
					oknowynik->ShowDialog();
				}	
				catch(Exception^ e){
					MessageBox::Show(e->Message);
				}
			}//endtry
			else if(pleck->Checked==true){
				try{			
					if(akm->Checked==true){
					setwsp(1.1);
					}
					else if(aks->Checked==true){
					setwsp(1.2);
					}
					else if(aknn->Checked==true){
					setwsp(1.3);
					}
					else if(akd->Checked==true){
					setwsp(1.4);
					}
					else{
						setwsp(1);
						MessageBox::Show("Wybierz rodzaj aktywno�ci, w ci�gu dnia.");
					}
				
					System::Int16 m1 = Convert::ToInt16(labelm1->Text);
					System::Int16 m2 = Convert::ToInt16(labelm2->Text);
				
					if(m1<m2){						
						oknowynik->lkcal->Text=Convert::ToString(m1*24*0.9*getwsp()+250); 
						oknowynik->gdymasamniejsza->Text="Masa nie ro�nie? Dodaj kalori :)";
					}
					else if(m1>m2){
						oknowynik->lkcal->Text=Convert::ToString(m1*24*0.9*getwsp()-400);
						oknowynik->gdymasamniejsza->Text="Podczas zrzucania wagi pami�taj o treningu ruchowym!";
					}
					else{
						oknowynik->lkcal->Text=Convert::ToString(m1*24*0.9*getwsp());
					}
					
					System::Double kalorie = Convert::ToDouble(oknowynik->lkcal->Text); 
					oknowynik->lwegle->Text=Convert::ToString(Convert::ToInt16(kalorie/8));
					oknowynik->lbialko->Text=Convert::ToString(Convert::ToInt16(kalorie*0.3/4));
					oknowynik->ltluszcze->Text=Convert::ToString(Convert::ToInt16(kalorie*0.2/9));
					oknowynik->ShowDialog();
				}//endtry	
				catch(Exception^ e){
					MessageBox::Show("Kobieta->"+e->Message);
				}			
			}//endelseif
			else{
				MessageBox::Show("Wybierz p�e�!");
			}
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 info^ informacja = gcnew info();		 
			 informacja->ShowDialog();
		 }
};
}

